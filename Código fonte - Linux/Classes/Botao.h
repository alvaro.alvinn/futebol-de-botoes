#ifndef BOTAO_H_INCLUDED
#define BOTAO_H_INCLUDED
#include "Animado.h"
#include "Time.h"
#include <string>

using namespace std;

class Botao :public Animado {
private:
    Vetor2D pos_ini;
    Time time;
    int raio;
    bool ativo;

public:
    Botao(double posX, double posY, int _raio, Time* t, int _massa,  bool _visivel);
    Botao();

    Time getTime();
    void setTime(Time t);

    bool getAtivo();
    void setAtivo(bool _ativo);

    int getRaio();
    void setRaio(int r);

    void movimenta();

    void setPosPadrao();

    string para_string();

};

Botao::Botao(double posX, double posY, int _raio, Time* t, int _massa, bool _visivel) {
    posicao = Vetor2D(posX, posY);
    pos_ini = posicao;
    raio = _raio;
    velocidade = Vetor2D();
    massa = _massa;
    //atrito ( 1 = sem atrito, 0 = atrito absoluto)
    aceleracao = Vetor2D(0.93, 0.93);
    movimento = false;
    time = *t;
    ativo = false;
    visivel = _visivel;
}

Botao::Botao() {
    posicao = Vetor2D();
    pos_ini = Vetor2D();
    raio = 0;
    velocidade = Vetor2D();
    massa = 0;
    //atrito ( 1 = sem atrito, 0 = atrito absoluto)
    aceleracao = Vetor2D(0.93, 0.93);
    movimento = false;
    time = Time();
    ativo = false;
    visivel = false;
}
Time Botao::getTime() {
    return time;
}

void Botao::setTime(Time t) {
    time = t;
}

bool Botao::getAtivo() {
    return ativo;
}

void Botao::setAtivo(bool _ativo) {
    ativo = _ativo;
}

int Botao::getRaio() {
    return raio;
}
void Botao::setRaio(int r) {
    raio = r;
}

void Botao::movimenta() {
    //atualiza a posicao do bot�o de acordo coma velocidade 
    setPosicao(posicao.vX + velocidade.vX, posicao.vY + velocidade.vY);

    // e a velocidade for extremamente baixa � definida como 0 e � alterado o status de movimento
    if (abs(velocidade.vX) <= 0.1 && abs(velocidade.vY) <= 0.1) {
        movimento = false;
        velocidade = Vetor2D();
    }
    else {
        movimento = true;
    }
    //se ainda esta em movimento a velocidade � diminuida pelo "atrito"
    if (movimento) {
        velocidade.vX = aceleracao.vX * velocidade.vX;
        velocidade.vY = aceleracao.vY * velocidade.vY;
    }

}

void Botao::setPosPadrao() {
    posicao = pos_ini;
    setVelocidade(0,0);
}

string Botao::para_string() {
    return to_string(posicao.vX) + "\n" + to_string(posicao.vY) + "\n";
}

#endif // CLASSES_H_INCLUDED

