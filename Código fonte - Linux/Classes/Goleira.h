#ifndef GOLEIRA_H_INCLUDED
#define GOLEIRA_H_INCLUDED
#include "Inanimado.h"
using namespace std;

class Goleira :public Inanimado {
private:
    int tamX;
    int tamY;
    int espessura;
    char lado; // 'd' = direita 'e' = esquerda

public:
    /// <summary>
    /// </summary>
    /// <param name="_lado">'e' = esquerda 'd' = direira</param>
    /// <param name="_tamX">tamanho do interior da goleira, da trave da frente at� a trave de tras</param>
    /// <param name="_tamY">tamanho da goleira de uma trava at� a outra</param>
    /// <param name="esp"> especura da trave</param>
    /// <param name="pos">posicao refernete a parte central somente do eixo y, e x = 0 </param>
    Goleira(char _lado, int _tamX, int _tamY, int esp, Vetor2D pos);
    int getX();
    int getY();
    int getEspessura();
    char getLado();

};

Goleira::Goleira(char _lado, int _tamX, int _tamY, int esp, Vetor2D pos) {
    tamX = _tamX;
    tamY = _tamY;
    espessura = esp;
    posicao = pos;
    visivel = true;
    lado = _lado;
}

int Goleira::getX() {
    return tamX;
}

int Goleira::getY() {
    return tamY;
}

int Goleira::getEspessura() {
    return espessura;
}

char Goleira::getLado() {
    return lado;
}

#endif // CLASSES_H_INCLUDED
#pragma once

