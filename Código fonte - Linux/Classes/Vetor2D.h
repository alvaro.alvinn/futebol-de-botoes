#ifndef VETOR2D_H_INCLUDED
#define VETOR2D_H_INCLUDED

#include<cmath>
using namespace std;

class Vetor2D {
public:
    double vX;
    double vY;
    Vetor2D(double x, double y);
    Vetor2D();
    Vetor2D unitario();
    Vetor2D soma(Vetor2D v_2);
    Vetor2D soma(double num);
    Vetor2D subtracao(Vetor2D v_2);
    Vetor2D subtracao(double num);
    Vetor2D multiplicacao(Vetor2D v_2);
    Vetor2D multiplicacao(double num);
    double escalar(Vetor2D v_2);
    Vetor2D divisao(double num);
};
Vetor2D::Vetor2D(double x, double y) {
    vX = x;
    vY = y;
}

Vetor2D::Vetor2D() {
    vX = 0;
    vY = 0;
}
Vetor2D Vetor2D::unitario() {
       double modulo = sqrt(pow(vX, 2) + pow(vY, 2));
       double u_x = vX / modulo;
       double u_y = vY / modulo;
       return Vetor2D(u_x, u_y);
}
Vetor2D Vetor2D::soma(Vetor2D v_2) {
    double novo_x = vX + v_2.vX;
    double novo_y = vY + v_2.vY;
    return Vetor2D(novo_x, novo_y);
}

Vetor2D Vetor2D::soma(double num) {
    double novo_x = vX + num;
    double novo_y = vY + num;
    return Vetor2D(novo_x, novo_y);
}

Vetor2D Vetor2D::subtracao(double num) {
    double novo_x = vX - num;
    double novo_y = vY - num;
    return Vetor2D(novo_x, novo_y);
}

Vetor2D Vetor2D::subtracao(Vetor2D v_2) {
    double novo_x = vX - v_2.vX;
    double novo_y = vY - v_2.vY;
    return Vetor2D(novo_x, novo_y);
}

Vetor2D Vetor2D::multiplicacao(Vetor2D v_2) {
    double novo_x = vX * v_2.vX;
    double novo_y = vY * v_2.vY;
    return Vetor2D(novo_x, novo_y);
}

Vetor2D Vetor2D::multiplicacao(double num) {
    double novo_x = vX * num;
    double novo_y = vY * num;
    return Vetor2D(novo_x, novo_y);
}

double Vetor2D::escalar(Vetor2D v_2) {
    double novo_x = vX * v_2.vX;
    double novo_y = vY * v_2.vY;
    return novo_x + novo_y;
}

Vetor2D Vetor2D::divisao(double num) {
    double novo_x = vX / num;
    double novo_y = vY / num;
    return Vetor2D(novo_x, novo_y);
}



#endif // CLASSES_H_INCLUDED

