#ifndef MOUSE_H_INCLUDED
#define MOUSE_H_INCLUDED
#include <cmath>
#include "Vetor2D.h"
#include "Botao.h"
#include <SDL2/SDL.h>



using namespace std;

class Mouse
{
private:
	//posicao esquerda direita cima baixo
	Vetor2D posicao;

	//vetor dire��o em rela��o ao jogador
	Vetor2D direcao;


public:
	//Initializes internal variables
	Mouse();

	Vetor2D getPosicao();

	//determina a direcao
	void setPosicao(double x, double y);

	//detrmina a dire�o 
	void setDirecao(double x, double y);

	//utilizado a posicao do mouse em rela�o ao jogador aplica a velocidade inicial
	Vetor2D Impulso(Botao jogaodor, double forca);

	//distancia entre botao e mouse
	double distancia(Botao jogador);

	//Atualiza posicao do mouse
	void update();


};

Mouse::Mouse()
{
	posicao.vX = 0;
	posicao.vY = 0;

	direcao.vX = 0;
	direcao.vY = 0;

	
}

Vetor2D Mouse::getPosicao()
{
	return posicao;
}

void Mouse::setPosicao(double x, double y)
{
	posicao = Vetor2D(x, y);
}

void Mouse::setDirecao(double x, double y)
{
	direcao = Vetor2D(x, y);

}

Vetor2D Mouse::Impulso(Botao jogador, double forca)
{
	//Get mouse position
	int x, y;
	Vetor2D vetor;
	double modulo;

	// as variaveis x e y recebem a localiza��o x e y do mouse
	SDL_GetMouseState(&x, &y);
	printf("posicao do mouse: %d %d\n", x, y);

	// define a posicao
	setPosicao(x, y);

	//
	x = x - (int)jogador.getPosicao().vX;
	y = y - (int)jogador.getPosicao().vY;

	modulo = sqrt(pow(x, 2) + pow(y, 2));
	printf("MODULO: %.2f\n",modulo);

	if (x == 0 && y == 0) {
		return Vetor2D();
	}

	printf("posicao RELATIVA do mouse: %d %d\n", x, y);

	//um vetor unitario do vetor do mouse ao jogador
	setDirecao(x /modulo, y /modulo);

	printf("DIRECAO: %.2f %.2f\n", direcao.vX, direcao.vY);

	printf("RESULTADO: %.2f %.2f\n", forca * direcao.vX, forca * direcao.vY);

	//� aplicada a forca ao vetor 
	vetor.vX = -(forca * direcao.vX);
	vetor.vY = -(forca * direcao.vY);

	// � retornado um vetor de velocidade que deve ser recebido pelo jogador ativo
	return vetor;
}

void Mouse::update() {
	int x, y;
	SDL_GetMouseState(&x, &y);
	setPosicao(x, y);
}

double Mouse::distancia(Botao jogador) {
	int x, y;
	SDL_GetMouseState(&x, &y);

	return sqrt(pow((jogador.getPosicao().vX - x),2) + pow((jogador.getPosicao().vY - y),2));
}




#endif // MOUSE_H_INCLUDED

#pragma once

