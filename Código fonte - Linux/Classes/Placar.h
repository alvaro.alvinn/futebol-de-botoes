#ifndef PLACAR_H_INCLUDED
#define PLACAR_H_INCLUDED
#include "Inanimado.h"
#include "Time.h"
#include "Texto.h"

using namespace std;

class Placar :public Inanimado {
private:
    Texto g1;
    Texto g2;
    int p1;
    int p2;
    bool gol;

public:
    Placar(int pos_x);
    void aumentaG1();
    void aumentaG2();
    void zera();
    Texto getG1();
    Texto getG2();
    void setG1(int g);
    void setG2(int g);
};

Placar::Placar(int pos_x) {

    posicao = Vetor2D(pos_x, 0);
    g1 = Texto("0", (int)posicao.vX - (int)posicao.vX  / 4 + 17, (int)posicao.vY, 50, true);
    g2 = Texto("0", (int)posicao.vX + (int)posicao.vX  / 12 + 17, (int)posicao.vY, 50, true);
    p1 = 0;
    p2 = 0;
    gol = false;
}


void Placar::aumentaG1() {
    p1 = p1 + 1;
    g1.setConteudo(to_string(p1));
}

void Placar::aumentaG2() {
    p2 = p2 + 1;
    g2.setConteudo(to_string(p2));
}

void Placar::zera() {
    p1 = 0;
    p2 = 0;
    g1.setConteudo(to_string(p1));
    g2.setConteudo(to_string(p1));
}

Texto Placar::getG1() {
    return g1;
}

Texto Placar::getG2() {
    return g2;
}

void Placar::setG1(int g) {
    p1 = g;
    g1.setConteudo(to_string(p1));
}

void Placar::setG2(int g) {
    p2 = g;
    g2.setConteudo(to_string(p2));
}



#endif // CLASSES_H_INCLUDED
#pragma once
