#ifndef TIME_H_INCLUDED
#define TIME_H_INCLUDED
#include <SDL2/SDL.h>
#include <string>

using namespace std;

class Time {
private:
public:
	string nome;
	Uint8 r;
	Uint8 g;
	Uint8 b;

	Time(string n, Uint8 _r, Uint8 _g, Uint8 _b);
	Time();
};

Time::Time(string n, Uint8 _r, Uint8 _g, Uint8 _b) {
	nome = n;
	r = _r;
	g = _g;
	b = _b;
}
Time::Time() {
	nome = "time sem nome";
	r = 0;
	g = 0;
	b = 0;
}


#endif // TIME_H_INCLUDED

#pragma once
