#ifndef TEXTO_H_INCLUDED
#define TEXTO_H_INCLUDED
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>
#include "Vetor2D.h"
#include "Inanimado.h"

using namespace std;

class Texto :public Inanimado {
private:

	string conteudo;
	const char* fonte = "l_10646.ttf";
	int tamanho_fonte;
	SDL_Color cor;

public:
	Texto(string c, int posX, int posY, bool _visivel);
	Texto(string c, int posX, int posY, int tam, bool _visivel);
	Texto();

	string getConteudo();
	void setConteudo(string c);

	SDL_Color getCor();
	void setCor(SDL_Color c);

	const char* getFonte();

	int getTamanho_fonte();
	void setTamanho_fonte(int t);

};

Texto::Texto(string c, int posX, int posY, bool _visivel) {
	conteudo = c;

	tamanho_fonte = 20;
	posicao = Vetor2D(posX, posY);
	cor = { 255,255,255 };
	visivel = _visivel;
}

Texto::Texto(string c, int posX, int posY, int tam, bool _visivel) {
	conteudo = c;
	tamanho_fonte = tam;
	posicao = Vetor2D(posX, posY);
	cor = { 255,255,255 };
	visivel = _visivel;
}

Texto::Texto() {
	conteudo = "";
	tamanho_fonte = 20;
	posicao = Vetor2D();
	cor = { 255,255,255 };
	visivel = false;
}


string Texto::getConteudo() {
	return conteudo;
}
void Texto::setConteudo(string c ) {
	conteudo = c;
}

SDL_Color Texto::getCor() {
	return cor;
}
void Texto::setCor(SDL_Color c) {
	cor = c;
}

const char* Texto::getFonte() {
	return fonte;
}

int Texto::getTamanho_fonte() {
	return tamanho_fonte;
}
void Texto::setTamanho_fonte(int t) {
	tamanho_fonte = t;
}





#endif // CLASSES_H_INCLUDED
