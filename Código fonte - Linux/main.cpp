// utilizando SDL, SDL image, E/S bésica, math, vector e SDL ttf.
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mouse.h>
#include <stdio.h>
#include <string>
#include <cmath>
#include <vector>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <fstream>

// utilizando classes e funcoes criadas
#include "Classes/Vetor2D.h"
#include "Classes/Animado.h"
#include "Classes/Inanimado.h"
#include "Classes/Botao.h"
#include "Classes/Campo.h"
#include "Classes/Placar.h"
#include "my_functions.h"
#include "Classes/Desenha.h"
#include "Classes/Colisao.h"
#include "Classes/Time.h"
#include "Classes/Mouse.h"
#include "Classes/Goleira.h"


//Cosntantes importantes
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
const int RAIO_J = 25;


//Inicia o SDL e cira uma janela
bool init();

//Loads media
bool loadMedia();

//Frees media and shuts down SDL
void close();

//Loads individual image as texture
SDL_Texture* loadTexture( std::string path );

int main(int argc, char* args[]);

//The window we'll be rendering to
SDL_Window* gWindow = NULL;

//The window renderer
SDL_Renderer* gRenderer = NULL;




bool init()
{
	//Initialization flag
	bool success = true;
	if (TTF_Init() < 0) {
		printf("NÃO foi possivel inicializar SDL_TTF!, algumas funéées podem não funcionar");
	}
	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else
	{
		//Set texture filtering to linear
		if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
		{
			printf( "Warning: Linear texture filtering not enabled!" );
		}

		//Create window
		gWindow = SDL_CreateWindow( "Futebol de botoes", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
		if( gWindow == NULL )
		{
			printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
			success = false;
		}
		else
		{
			//Create renderer for window
			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
			if( gRenderer == NULL )
			{
				printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
				success = false;
			}
			else
			{
				//Initialize renderer color
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if( !( IMG_Init( imgFlags ) & imgFlags ) )
				{
					printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
					success = false;
				}
			}
		}
	}

	return success;
}

bool loadMedia()
{
	//Loading success flag
	bool success = true;

	//Nothing to load
	return success;
}

void close()
{
	//Destroy window	
	SDL_DestroyRenderer( gRenderer );
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;
	gRenderer = NULL;

	//Quit SDL subsystems
	IMG_Quit();
	TTF_Quit();
	SDL_Quit();
}

SDL_Texture* loadTexture( std::string path )
{
	//The final texture
	SDL_Texture* newTexture = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load( path.c_str() );
	if( loadedSurface == NULL )
	{
		printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
	}
	else
	{
		//Create texture from surface pixels
        newTexture = SDL_CreateTextureFromSurface( gRenderer, loadedSurface );
		if( newTexture == NULL )
		{
			printf( "Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
		}

		//Get rid of old loaded surface
		SDL_FreeSurface( loadedSurface );
	}

	return newTexture;
}


int main( int argc, char* args[] )
{
	//Start up SDL and create window
	if( !init() )
	{
		printf( "Failed to initialize!\n" );
	}
	else
	{
		//Load media
		if( !loadMedia() )
		{
			printf( "Failed to load media!\n" );
		}
		else
		{	
			//Main loop flag
			bool quit = false;

			//Event handler
			SDL_Event e;
			SDL_Event ultimo_e;

			//variaveis relacionadas ao tempo
			Uint32 tempo_antigo = SDL_GetTicks(), tempo_atual ;
			Uint32 tempo_em_s;
			Uint32 tempo_em_s_salvo = 0;
			string seg;
			string min;

			//variaveis de estado do jogo
			string estado_rpincipal;

			//o jogo sempre inicia com o jogador 1 
			//Opções: vez_de_1, vez_de_2, pronto_para_jogar, movimento;
			string sub_estado = "inicio";
			string proximo_sub_estado = "inicio";

			//Instancia Mouse
			Mouse* mouse = new Mouse();

			// Instancia objeto desenha
			Desenha* renderizador = new Desenha(gRenderer);

			// Inicia Colisao
			Colisao* colisao = new Colisao();

			//Instancia times
			Time* time_1 = new Time("Time 1", 40, 180, 200);
			Time* time_2 = new Time("Time 2", 199, 72, 50);
			Time* time_bola = new Time("", 89, 89, 83);

			//Instancia placar
			Placar* placar = new Placar(SCREEN_WIDTH/2);

			// Instancia objetos botÃO
			Botao* jogador_1 = new Botao((int)(SCREEN_WIDTH / 12 + SCREEN_WIDTH / 12), SCREEN_HEIGHT / 2, RAIO_J, time_1, 50, true);
			Botao* jogador_2 = new Botao((int)(SCREEN_WIDTH / 12 + SCREEN_WIDTH / 5) , 2* SCREEN_HEIGHT / 8 , RAIO_J, time_1, 50, true);
			Botao* jogador_3 = new Botao((int)(SCREEN_WIDTH / 12 + SCREEN_WIDTH / 5), (int)(SCREEN_HEIGHT - 2 * SCREEN_HEIGHT / 8 ) , RAIO_J, time_1, 50, true);
			Botao* jogador_4 = new Botao((int)(SCREEN_WIDTH / 12 + SCREEN_WIDTH / 3.5) , SCREEN_HEIGHT / 2, RAIO_J, time_1, 50, true);

			Botao* jogador_5 = new Botao((int)(SCREEN_WIDTH - (SCREEN_WIDTH/12 + SCREEN_WIDTH / 12)), SCREEN_HEIGHT /2, RAIO_J, time_2, 50, true);
			Botao* jogador_6 = new Botao((int)(SCREEN_WIDTH - (SCREEN_WIDTH / 12 + SCREEN_WIDTH / 5 )), 2* SCREEN_HEIGHT / 8 , RAIO_J, time_2, 50, true);
			Botao* jogador_7 = new Botao((int)(SCREEN_WIDTH - (SCREEN_WIDTH / 12 + SCREEN_WIDTH / 5)), (int)(SCREEN_HEIGHT - 2 * SCREEN_HEIGHT / 8  ), RAIO_J, time_2, 50, true);
			Botao* jogador_8 = new Botao((int)(SCREEN_WIDTH - (SCREEN_WIDTH / 12 + SCREEN_WIDTH / 3.5)) , SCREEN_HEIGHT / 2, RAIO_J, time_2, 50, true);
			
			//Instancia bola
			Botao* bola = new Botao(SCREEN_WIDTH/2, SCREEN_HEIGHT/2, 10, time_bola, 20, true);

			//Vector com todos os ativos (botoese bola)
			vector<Botao> ativos;
			ativos.push_back(*jogador_1);
			ativos.push_back(*jogador_2);
			ativos.push_back(*jogador_3);
			ativos.push_back(*jogador_4);
			ativos.push_back(*jogador_5);
			ativos.push_back(*jogador_6);
			ativos.push_back(*jogador_7);
			ativos.push_back(*jogador_8);

			ativos.push_back(*bola);

			size_t total = ativos.size();
			int a = 0; //id do jogador ativo IMPORTANTE

			// Instancia objeto campo
			Campo* campo = new Campo(SCREEN_WIDTH, SCREEN_HEIGHT);
			// Instancia goleiras
			Goleira* goleira_D = new Goleira('d', 35, 65, 7, Vetor2D((int)(SCREEN_WIDTH - (SCREEN_WIDTH / 12)), SCREEN_HEIGHT / 2));
			Goleira* goleira_E = new Goleira('e', 35, 65, 7, Vetor2D(SCREEN_WIDTH / 12, SCREEN_HEIGHT / 2));
			Goleira* goleira_proxima;

			// Instancia Textos
			Texto* texto_superior_esquerda = new Texto("FORÇA:", 10, 10, true);
			Texto* relogio = new Texto("00:00", SCREEN_WIDTH / 2 - 28, 40, true);

			Texto* info = new Texto("", SCREEN_WIDTH / 2 - 220, SCREEN_HEIGHT - 50, false);
			Texto* jogue = new Texto("JOGUE!", SCREEN_WIDTH / 2 - 30, SCREEN_HEIGHT - 50, false);
			Texto* continuar = new Texto("GOSTARIA DE CONTINUAR PARTIDA ANTERIOR? S/N", SCREEN_WIDTH / 5.5 , SCREEN_HEIGHT/1.7, true);

			// inicia variaveis importantes
			bool pronto_para_jogar = true;
			int j_da_vez = 1;
			int jogador_atual = 0;
			bool fim_de_jogada = false;

			//arquvo de save
			fstream savefile;

			//variaveis auxiliares
			int i = 0;
			int j = 0;
			int k = 0;

			// foréa: de 1 até 50 !!
			bool mais_forca = true;
			double forca = 0;


			// While principal
			while (!quit)
			{	
				bool clicou = false;
				// Gerencia tempo
				if (sub_estado != "inicio") {
					tempo_atual = SDL_GetTicks();
					tempo_em_s = ((tempo_atual - tempo_antigo) / 1000) + tempo_em_s_salvo;
					i = (int)(tempo_em_s / 60);
					j = (int)((int)tempo_em_s % 60);
					if (i > 9) {
						min = to_string(i);
					}
					else {
						min = "0" + to_string(i);
					}
					if (j > 9) {
						seg = to_string(j);
					}
					else {
						seg = "0" + to_string(j);
					}
					relogio->setConteudo(min + ":" + seg);
				}

				//gerenciador de eventos
				while (SDL_PollEvent(&e) != 0)
				{
					//sair
					if (e.type == SDL_QUIT)
					{
						quit = true;
					}

					if((e.type == SDL_MOUSEBUTTONUP)){
						clicou = true;
					}


					if (sub_estado == "inicio") {
						if (e.type == SDL_KEYDOWN) {
							if (e.key.keysym.sym == SDLK_s) {
								string x;
								string y;
								string aux;

								savefile.open("ultimo_jogo.txt", ios::in);
								if (savefile.is_open()) {
									cout << "ARQUIVO ABERTO COM SUCESSO!\n";
									//recupera posicao dos jogadores
									for (i = 0; i < total; i++) {
										for (k = 0; k < 2; k++) {
											if (k == 0) {
												getline(savefile, x);
											}
											else {
												getline(savefile, y);
											}
										}
										ativos[i].setPosicao(stod(x), stod(y));
									}
									//recupera pontuacao do time 1
									getline(savefile, aux);
									placar->setG1(stoi(aux));
									//recupera pontuacao do time 2
									getline(savefile, aux);
									placar->setG2(stoi(aux));
									//recupera tempo de jogo
									getline(savefile, aux);
									tempo_em_s_salvo = (Uint32)stoi(aux);
									//recupera de quem e a vez de jogar
									getline(savefile, aux);
									sub_estado = aux;

									continuar->setVisivel(false);
									savefile.close();
								}
								else {
									cout << "Erro ao abrir o arquivo ou nao ha registros de jogo anterior";
									sub_estado = "vez_de_1";
									continuar->setVisivel(false);
								}
							}
							else if(e.key.keysym.sym == SDLK_n) {
								sub_estado = "vez_de_1";
								continuar->setVisivel(false);
							}
						}
					}
					//savla o ultimo evento
					ultimo_e = e;
				}
				if (sub_estado == "vez_de_1") {
					proximo_sub_estado = "vez_de_1";
					forca = 0;
					info->setConteudo("VEZ DO JOGADOR 1 SELECIONAR O BOTAO!");
					info->setVisivel(true);
					jogador_atual = 1;
					// o mais proximo do mouse é o ativo
					double menor = 100000;
					//backspace para voltar uma jogada
					for (i = 0; i < (total - 1); i++) {
						if (ativos[i].getTime().nome == "Time 1") {
							if (mouse->distancia(ativos[i]) < menor) {
								menor = mouse->distancia(ativos[i]);
							
								ativos[a].setAtivo(false);
								
								a = i;
							}
						}
					}
					ativos[a].setAtivo(true);
					//se o clique ocorre o jogador mais proximo é selecionado 
					if (clicou) {
						proximo_sub_estado = "pronto_para_jogar";
						pronto_para_jogar = false;
					}
				}
				if (sub_estado == "vez_de_2") {
					proximo_sub_estado = "vez_de_2";
					forca = 0;					
					info->setConteudo("VEZ DO JOGADOR 2 SELECIONAR O BOTAO!");
					info->setVisivel(true);
					jogador_atual = 2;
					// o mais proximo do mouse é o ativo
					double menor = 100000;
					for (i = 0; i < (total - 1); i++) {
						if (ativos[i].getTime().nome == "Time 2") {
							if (mouse->distancia(ativos[i]) < menor) {
								menor = mouse->distancia(ativos[i]);
								ativos[a].setAtivo(false);
								a = i;
							}
						}
					}
					ativos[a].setAtivo(true);
					//se o clique ocorre o jogador mais proximo é selecionado
					if (clicou) {
						proximo_sub_estado = "pronto_para_jogar";
						pronto_para_jogar = true;
					}
				}
				if (sub_estado == "pronto_para_jogar") {
					proximo_sub_estado = "pronto_para_jogar";
					info->setVisivel(false);
					jogue->setVisivel(true);
					if (ultimo_e.type == SDL_MOUSEBUTTONDOWN) {
						pronto_para_jogar = true;
					}
					// MOVIMENTO POR CLICK DO MOUSE					
					for (i = 0; i < total; i++) {
						// iteração somente com o botão selecionado como ativo pelo usuario
						if (ativos[i].getAtivo()) {
							if ((clicou)) {
								ativos[i].setVelocidade(mouse->Impulso(ativos[i], forca));
								pronto_para_jogar = false;
								ativos[i].setAtivo(false);
								proximo_sub_estado = "movimento";
								jogue->setVisivel(false);					
								cout << ativos[0].getMovimento() << endl;
							}
						}
					}
					//TRATAMENTO DA FORÇA
					if (!jogador_1->getMovimento()) {
						if (mais_forca) {
							if (forca >= 49) {
								forca -= 1;
								mais_forca = false;
							}
							forca += 1;
						}
						else {
							if (forca <= 1) {
								forca += 1;
								mais_forca = true;
							}
							forca -= 1;
						}
					}
				}
				
				// ESTADO DE MOVIMENTO
				if (sub_estado == "movimento") {
					proximo_sub_estado = "movimento";
					//FÍSICA ENTRE OBJETOS
					fim_de_jogada = false;
					bool todos_parados = true;
					for (i = 0; i < total; i++) {
						
						//define a goleira mais proxima
						if (ativos[i].getPosicao().vX > SCREEN_WIDTH / 2) {
							goleira_proxima = goleira_D;
						}
						else {
							goleira_proxima = goleira_E;
						}
						
						//teste de colisÃO com a goleira
						if (colisao->esta_dentro(ativos[i], *goleira_proxima)) {
							colisao->colide(ativos[i], *goleira_proxima);
						}
						//caso não colida com a goleira teste de colisao com o campo
						else {
							colisao->colide(ativos[i], *campo);
						}
						//detecção de colisao entre jogadores 
						for (j = i + 1; j < total; j++) {
							//printf("b\n");
							if (colisao->colide(ativos[i], ativos[j])) {
								cout << endl;
								colisao->Impulso(ativos[i], ativos[j]);
							}
						}

						// movimenta 
						if (ativos[i].getMovimento()) {
							//printf("c\n");
							ativos[i].movimenta();
						}

						//determina se não ha mais moviemnto
						if (ativos[i].getMovimento() && todos_parados) {
							todos_parados = false;
						}
						
						//ultimo movimento do frame
						if (i == (total - 1)) {
							//detecção de gol
							if (colisao->esta_inteiro_dentro(ativos[i], *goleira_proxima)) {
								cout << goleira_proxima->getLado() << endl;
								if (goleira_proxima->getLado() == 'd') {
									placar->aumentaG1();
									proximo_sub_estado = "vez_de_2";
									//para não entrar no próximo if
									todos_parados = false;
								}
								else if (goleira_proxima->getLado() == 'e') {
									placar->aumentaG2();
									proximo_sub_estado = "vez_de_1";
									//para não entrar no próxmo if
									todos_parados = false;

								}
								for (k = 0; k < total; k++) {
									ativos[k].setPosPadrao();
								}
								fim_de_jogada = true;
							}
							//se estão todos paradao sai do estado de moviemnto
							if (todos_parados) {
								if (jogador_atual == 1) {
									proximo_sub_estado = "vez_de_2";
								}
								else {
									proximo_sub_estado = "vez_de_1";
								}
								fim_de_jogada = true;
							}
						}
					}
					//salva 
					if (fim_de_jogada) {
						savefile.open("ultimo_jogo.txt", ios::out);
						//grava posicoes dos bototes
						for (k = 0; k < total; k++) {
							savefile << ativos[k].para_string();
						}
						//grava gols
						savefile << placar->getG1().getConteudo() << "\n" << placar->getG2().getConteudo() << endl;
						//grava tempo
						savefile << tempo_em_s << endl;
						//grava sub estado (de  quem é a vez de jogar)
						savefile << sub_estado;
						savefile.close();
					}
				}				
				//converte o valor da força para seu objeto de texto
				texto_superior_esquerda->setConteudo(string("FORCA: ") + to_string((int)forca) + "." + to_string((int)((forca - (int)forca) * 10)));

				sub_estado = proximo_sub_estado;

				//limpa a tela para desenhar o próximo frame, com o fundo cinza
				SDL_SetRenderDrawColor(gRenderer, 0x80, 0x80, 0x80, 0x80);
				SDL_RenderClear(gRenderer);

				//desenha o campo
				renderizador->renderiza(*campo);

				//desenha o placar
				renderizador->renderiza(*placar, *time_1, *time_2);

				//desenha goleiras
				renderizador->renderiza(*goleira_D);
				renderizador->renderiza(*goleira_E);

				//desenha o jogadores
				for (i = 0; i < total; i++) {
					if (ativos[i].getVisivel()) {
						renderizador->renderiza(ativos[i]);
					}
				}

				//desenha textos
				renderizador->renderiza(*texto_superior_esquerda);
				renderizador->renderiza(*relogio);

				//desenha barra de força
				renderizador->renderiza_forca(*texto_superior_esquerda, forca);

				renderizador->renderiza(*info);
				renderizador->renderiza(*jogue);
				renderizador->renderiza(*continuar);
			
				//Atualiza a tela
				SDL_RenderPresent(gRenderer);
				
			}
		}
	}
	//Free resources and close SDL
	close();

	return 0;
}