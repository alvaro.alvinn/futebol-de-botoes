#ifndef ANIMADO_H_INCLUDED
#define ANIMADO_H_INCLUDED
#include "Vetor2D.h"


using namespace std;

class Animado {
protected:

    Vetor2D posicao;
    Vetor2D aceleracao;
    Vetor2D velocidade;
    int massa;
    bool movimento;
    bool visivel;

public:

    Animado();

    Vetor2D getPosicao();
    void setPosicao(double x, double y);

    Vetor2D getAceleracao();
    void setAceleracao(double x, double y);

    Vetor2D getVelocidade();
    void setVelocidade(double x, double y);
    void setVelocidade(Vetor2D vetor);

    int getMassa();
    void setMassa(int m);

    bool getMovimento();
    void setMovimento(bool m);

    bool getVisivel();
    void setVisivel(bool v);

};

Animado::Animado() {
    posicao = Vetor2D();
    aceleracao = Vetor2D();
    velocidade = Vetor2D();
    massa = 0;
    movimento = false;
    visivel = false;
}

Vetor2D Animado::getPosicao() {
    return posicao;
};

void Animado::setPosicao(double x, double y) {
    if (x > 800) {
        x = posicao.vX - 100;
        setVelocidade(velocidade.vX / 2, velocidade.vY / 2);

    }
    if (x < 0) {
        x = posicao.vX + 100;
        setVelocidade(velocidade.vX / 2, velocidade.vY / 2);

    }
    if (y > 600) {
        y = posicao.vY - 100;
        setVelocidade(velocidade.vX / 2, velocidade.vY / 2);

    }    
    if (y < 0) {
        y = posicao.vY + 100;
        setVelocidade(velocidade.vX / 2, velocidade.vY / 2);

    }
    posicao = Vetor2D(x, y);
};

Vetor2D Animado::getAceleracao() {
    return aceleracao;
};

void Animado::setAceleracao(double x, double y) {
    aceleracao = Vetor2D(x, y);
};

Vetor2D Animado::getVelocidade() {
    return velocidade;
}

void Animado::setVelocidade(double x, double y) {
    velocidade = Vetor2D(x, y);
    if (x != 0 || y != 0) {
        movimento = true;
    }
    else {
        movimento = false;
    }
};

void Animado::setVelocidade(Vetor2D vetor) {
    velocidade = vetor;
    if (vetor.vX != 0 || vetor.vY != 0) {
        movimento = true;
    }
    else{
        movimento = false;
    }
};

int Animado::getMassa() {
    return massa;
};

void Animado::setMassa(int m) {
    massa = m;
};


bool Animado::getMovimento() {
    return movimento;
};

void Animado::setMovimento(bool m) {
    movimento = m;
};

bool Animado::getVisivel() {
    return visivel;
};

void Animado::setVisivel(bool v) {
    visivel = v;
};

#endif // CLASSES_H_INCLUDED

