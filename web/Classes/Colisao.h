#ifndef COLISAO_H_INCLUDED
#define COLISAO_H_INCLUDED
#include "Botao.h"
#include "Campo.h"
#include "Goleira.h"


using namespace std;

class Colisao {
private:
   
public:
	Colisao();
	
	//Verifica a colis�o entre botoes
	bool colide(Botao jogador_1, Botao jogador_2);

	//verifica e aplica colis�o dos botoes com o campo
	bool colide(Botao &jogador, Campo &campo);

	//verifica e aplica colis�o entre jogadores e goleira
	bool colide(Botao &jogador, Goleira &goleira);

	//verifica se o bot�o esta na area interna da goleira
	bool esta_dentro(Botao jogador, Goleira goleira);

	//verifica se o bot�o esta inteiramente na area interna da goleira
	bool esta_inteiro_dentro(Botao jogador, Goleira goleira);

	//aplica colisao entre botoes
	void Impulso(Botao &jogador_1, Botao &jogador_2);

};

Colisao::Colisao() {

}

bool Colisao::colide(Botao jogador_1, Botao jogador_2) {
	// identifica se dois bot�es est�o entrando em contato  calculando a distancia entre sesu pontos de origime diminuindo o raio de cada bot�o
	double distancia = sqrt((pow((jogador_1.getPosicao().vX +  jogador_1.getVelocidade().vX - jogador_2.getPosicao().vX - jogador_2.getVelocidade().vX), 2)) + pow((jogador_1.getPosicao().vY +  jogador_1.getVelocidade().vY - jogador_2.getPosicao().vY - jogador_2.getVelocidade().vY), 2));
	if (distancia < (double) jogador_1.getRaio() + (double) jogador_2.getRaio()) {
		printf("DISTANCIA: %.2f\n", distancia - ((double)jogador_1.getRaio() + (double)jogador_2.getRaio()));
		return true;
	}
	else {
		return false;
	}
}

bool Colisao::colide(Botao &jogador, Campo &campo) {
	double distancia;
	// verifica se haver� colis�o entre um bot�o e as extremidades do campo 
	//estremidade esquerda
	distancia = abs(jogador.getPosicao().vX + jogador.getVelocidade().vX - (campo.getX() / 12));
	if (distancia < jogador.getRaio() || (jogador.getPosicao().vX + jogador.getVelocidade().vX + jogador.getRaio()-1) < campo.getX()/12) {
		printf("Colisao esquerda : %f\n", distancia);
		jogador.setVelocidade(-jogador.getVelocidade().vX, jogador.getVelocidade().vY);;
		return true;
	}
	//estremidade direita
	distancia = abs(jogador.getPosicao().vX +  jogador.getVelocidade().vX - (campo.getX() - (campo.getX() / 12)));
	if (distancia < jogador.getRaio() || (jogador.getPosicao().vX + jogador.getVelocidade().vX + jogador.getRaio() + 1) > (campo.getX() - (campo.getX() / 12))) {
		printf("Colisao direita : %f\n", distancia);
		jogador.setVelocidade(-jogador.getVelocidade().vX, jogador.getVelocidade().vY);
		return true;
	}
	//extremidade superior
	distancia = abs(jogador.getPosicao().vY + jogador.getVelocidade().vY - (campo.getY() / 8));
	if (distancia < jogador.getRaio() || (jogador.getPosicao().vY + jogador.getVelocidade().vY + jogador.getRaio() - 1) < campo.getY() / 8) {
		printf("Colisao topo : %f\n", distancia);
		jogador.setVelocidade(jogador.getVelocidade().vX, -jogador.getVelocidade().vY);
		return true;
	}
	//estremidade inferior
	distancia = abs(jogador.getPosicao().vY + jogador.getVelocidade().vY - (campo.getY() - (campo.getY() / 8)));
	if (distancia < jogador.getRaio() || (jogador.getPosicao().vY + jogador.getVelocidade().vY + jogador.getRaio() + 1) > (campo.getY() - (campo.getY() / 8))) {
		printf("Colisao baixo : %f\n", distancia);
		jogador.setVelocidade(jogador.getVelocidade().vX, -jogador.getVelocidade().vY);
		return true;
	}
	else {
		return false;
	}
}

bool Colisao::colide(Botao &jogador, Goleira &goleira) {
	double distancia, distancia_1, distancia_2;
	if (goleira.getLado() == 'e') {
		//parte de traz da goleira
		distancia = abs(jogador.getPosicao().vX + jogador.getVelocidade().vX - (goleira.getPosicao().vX - goleira.getX()));
		if (distancia < jogador.getRaio()) {
			printf("colisao atras da goleira\n");
			jogador.setVelocidade(-jogador.getVelocidade().vX, jogador.getVelocidade().vY);
			return true;
		}
		//parte de cima ou de baixo
		distancia_1 = abs(jogador.getPosicao().vY + jogador.getVelocidade().vY - (goleira.getPosicao().vY + goleira.getY()));
		distancia_2 = abs(jogador.getPosicao().vY + jogador.getVelocidade().vY - (goleira.getPosicao().vY - goleira.getY()));
		printf("Distancia 1: %.f distancia 2: %.f raio: %d\n", distancia_1, distancia_2, jogador.getRaio());
		if (distancia_1 <= jogador.getRaio()) {
			printf("colisao baixo da goleira\n");
			jogador.setVelocidade(jogador.getVelocidade().vX, -jogador.getVelocidade().vY);
			return true;
		}
		if (distancia_2 <= jogador.getRaio()) {
			printf("colisao cima da goleira\n");
			jogador.setVelocidade(jogador.getVelocidade().vX, -jogador.getVelocidade().vY);
			return true;
		}
		return false;
	}
	else {
		//parte de traz da goleira
		distancia = abs(jogador.getPosicao().vX + jogador.getVelocidade().vX - (goleira.getPosicao().vX + goleira.getX()));
		if (distancia < jogador.getRaio()) {
			printf("colisao atras da goleira\n");
			jogador.setVelocidade(-jogador.getVelocidade().vX, jogador.getVelocidade().vY);
			return true;
		}
		//parte de cima ou de baixo
		distancia_1 = abs(jogador.getPosicao().vY + jogador.getVelocidade().vY - (goleira.getPosicao().vY - goleira.getY()));
		distancia_2 = abs(jogador.getPosicao().vY + jogador.getVelocidade().vY - (goleira.getPosicao().vY + goleira.getY()));
		printf("Distancia 1: %.f distancia 2: %.f raio: %d\n", distancia_1, distancia_2, jogador.getRaio());
		if (distancia_1 <= jogador.getRaio()) {
			printf("colisao baixo da goleira\n");
			jogador.setVelocidade(jogador.getVelocidade().vX, -jogador.getVelocidade().vY);
			return true;
		}
		if (distancia_2 <= jogador.getRaio()) {
			printf("colisao cima da goleira\n");
			jogador.setVelocidade(jogador.getVelocidade().vX, -jogador.getVelocidade().vY);
			return true;
		}
		return false;
	}
	return false;
}

bool Colisao::esta_dentro(Botao jogador, Goleira goleira) {
	bool pertence_X;
	bool pertence_Y;
	if (goleira.getLado() == 'e') {
		pertence_X = ((jogador.getPosicao().vX - jogador.getRaio() + jogador.getVelocidade().vX) <= goleira.getPosicao().vX);
		pertence_Y = ((jogador.getPosicao().vY - jogador.getRaio()) >= (goleira.getPosicao().vY - goleira.getY())) &&
			((jogador.getPosicao().vY + jogador.getRaio()) <= (goleira.getPosicao().vY + goleira.getY()));
		if (pertence_X && pertence_Y) {
			printf("ESTA DENTRO\n");
			return true;
		}
		else {
			return false;
		}
	}
	else {
		pertence_X = ((jogador.getPosicao().vX + jogador.getRaio() + jogador.getVelocidade().vX) >= goleira.getPosicao().vX);
		pertence_Y = ((jogador.getPosicao().vY - jogador.getRaio()) >= (goleira.getPosicao().vY - goleira.getY())) &&
			((jogador.getPosicao().vY + jogador.getRaio()) <= (goleira.getPosicao().vY + goleira.getY()));
		if (pertence_X && pertence_Y) {
			printf("ESTA DENTRO\n");
			return true;
		}
		else {
			return false;
		}
	}
}

bool Colisao::esta_inteiro_dentro(Botao jogador, Goleira goleira) {
	bool pertence_X;
	bool pertence_Y;
	if (goleira.getLado() == 'e') {
		pertence_X = ((jogador.getPosicao().vX +  jogador.getRaio()) <= goleira.getPosicao().vX);
		pertence_Y = ((jogador.getPosicao().vY - jogador.getRaio()) >= (goleira.getPosicao().vY - goleira.getY())) &&
			((jogador.getPosicao().vY + jogador.getRaio()) <= (goleira.getPosicao().vY + goleira.getY()));
		if (pertence_X && pertence_Y) {
			printf("ESTA TODO DENTRO ESQUERDA\n");
			return true;
		}
		else {
			return false;
		}
	}
	else {
		pertence_X = ((jogador.getPosicao().vX + jogador.getRaio()) >= goleira.getPosicao().vX);
		pertence_Y = ((jogador.getPosicao().vY - jogador.getRaio()) >= (goleira.getPosicao().vY - goleira.getY())) &&
			((jogador.getPosicao().vY + jogador.getRaio()) <= (goleira.getPosicao().vY + goleira.getY()));
		if (pertence_X && pertence_Y) {
			printf("ESTA TODO DENTRO DIREITA\n");
			return true;
		}
		else {
			return false;
		}
	}
}

void Colisao::Impulso(Botao &jogador_1, Botao &jogador_2){
	//printf("-------------------------COLISAO---------------------\n");
	//obten��o do vetor normal
	Vetor2D vet_normal = Vetor2D(jogador_1.getPosicao().vX - jogador_2.getPosicao().vX, jogador_1.getPosicao().vY - jogador_2.getPosicao().vY);
	// vetor normal unitario
	Vetor2D n_unitario = vet_normal.unitario();
	//proje��es escalares
	double e_v1 = jogador_1.getVelocidade().escalar(n_unitario);
	double e_v2 = jogador_2.getVelocidade().escalar(n_unitario);

	//proje��es vetoriais
	Vetor2D v_v1_n = n_unitario.multiplicacao(e_v1);
	Vetor2D v_v2_n = n_unitario.multiplicacao(e_v2);
	//printf("Proje��es escalares de jogador 1: %.1f jogador 2: %.1f\n", e_v1, e_v2);

	//velocidade tangencial
	Vetor2D v1_t = jogador_1.getVelocidade().subtracao(v_v1_n);
	Vetor2D v2_t = jogador_2.getVelocidade().subtracao(v_v2_n);
	//printf("Velocidade tangencial de jogador 1: (%.1f, %.1f) jogador 2: (%.1f, %.1f)\n", v1_t.vX, v1_t.vY, v2_t.vX, v2_t.vY);

	//velocidades unidimensionais
	double e_v1_f = (jogador_1.getMassa() - jogador_2.getMassa()) / (jogador_1.getMassa() + jogador_2.getMassa()) * e_v1 + 2 * jogador_2.getMassa() * e_v2 / (jogador_1.getMassa() + jogador_2.getMassa());
	double e_v2_f = (jogador_2.getMassa() - jogador_1.getMassa()) / (jogador_2.getMassa() + jogador_1.getMassa()) * e_v2 + 2 * jogador_1.getMassa() * e_v1 / (jogador_2.getMassa() + jogador_1.getMassa());
	//printf("Velocidade unidimensional de jogador 1: %.1f jogador 2: %.1f\n", e_v1_f, e_v2_f);

	// aplicando normais as velocidades
	Vetor2D v_v1_nf = n_unitario.multiplicacao(e_v1_f);
	Vetor2D v_v2_nf = n_unitario.multiplicacao(e_v2_f);

	// atribui��o
	jogador_1.setVelocidade(v_v1_nf.soma(v1_t));
	jogador_2.setVelocidade(v_v2_nf.soma(v2_t));

	jogador_1.setMovimento(true);
	jogador_2.setMovimento(true);
	//printf("--------------------FIM DE COLISAO-------------------\n");
}


#endif // COLISAO_H_INCLUDED

#pragma once
