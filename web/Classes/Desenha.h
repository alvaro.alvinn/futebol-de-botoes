#ifndef DESENHA_H_INCLUDED
#define DESENHA_H_INCLUDED
#include <SDL2/SDL.h>
#include "SDL2/SDL_ttf.h"
#include "Botao.h"
#include "Campo.h"
#include "Placar.h"
#include "Texto.h"
#include "Goleira.h"
#include "../my_functions.h"
#include <iostream>

using namespace std;

class Desenha {
private:
	SDL_Renderer* renderizador;

public:
	Desenha(SDL_Renderer* _renderizador);

	void renderiza(Botao jogador);
	void renderiza(Botao jogador, Uint8 r, Uint8 g, Uint8 b);
	void renderiza(Campo campo);
	void renderiza(Texto texto);
	void renderiza(Goleira goleira);
	void renderiza(Placar placar, Time time_1, Time time_2);
	void renderiza_forca(Texto texto, double forca);
	


};

Desenha::Desenha(SDL_Renderer* _renderizador) {
	renderizador = _renderizador;
}

void Desenha::renderiza(Botao jogador) {

	if (jogador.getAtivo()) {
		//define a cor do contorno(branco)
		SDL_SetRenderDrawBlendMode(renderizador, SDL_BLENDMODE_BLEND);
		SDL_SetRenderDrawColor(renderizador, 255, 255, 255, 100);
		//desenha o contorno
		RenderFillCircle(renderizador, (int)jogador.getPosicao().vX, (int)jogador.getPosicao().vY, (int)jogador.getRaio() + 4);
		SDL_SetRenderDrawBlendMode(renderizador, SDL_BLENDMODE_NONE);
	}

	//define a cor do contorno(cinza)
	SDL_SetRenderDrawColor(renderizador, 80, 80, 80, 255);
	//desenha o contorno
	RenderFillCircle(renderizador, (int)jogador.getPosicao().vX, (int)jogador.getPosicao().vY, (int)jogador.getRaio());

	//define a cor principal
	SDL_SetRenderDrawColor(renderizador, (int)jogador.getTime().r, (int)jogador.getTime().g, (int)jogador.getTime().b, 255);
	//preenche  o jogador coma cor principal
	RenderFillCircle(renderizador, (int)jogador.getPosicao().vX, (int)jogador.getPosicao().vY, (int)jogador.getRaio() - 3);

	//desenha vetor de dire��o para debug
	SDL_SetRenderDrawColor(renderizador, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderDrawLine(renderizador, (int)jogador.getPosicao().vX, (int)jogador.getPosicao().vY, (int)jogador.getPosicao().vX + (int)jogador.getVelocidade().vX * 5, (int)jogador.getPosicao().vY + (int)jogador.getVelocidade().vY * 5);
}

void Desenha::renderiza(Botao jogador, Uint8 r, Uint8 g, Uint8 b) {
	//define a cor do contorno(cinza)
	SDL_SetRenderDrawColor(renderizador, 80, 80, 80, 255);

	//desenha o contorno
	RenderFillCircle(renderizador, (int)jogador.getPosicao().vX, (int)jogador.getPosicao().vY, (int)jogador.getRaio());

	//define a cor principal
	SDL_SetRenderDrawColor(renderizador, r, g, b, 255);

	//preenche  o jogador coma cor principal
	RenderFillCircle(renderizador, (int)jogador.getPosicao().vX, (int)jogador.getPosicao().vY, (int)jogador.getRaio() - 3);

	//desenha vetor de dire��o para debug
	SDL_SetRenderDrawColor(renderizador, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderDrawLine(renderizador, (int)jogador.getPosicao().vX, (int)jogador.getPosicao().vY , (int)jogador.getPosicao().vX + (int)jogador.getVelocidade().vX * 10, (int)jogador.getPosicao().vY + (int)jogador.getVelocidade().vY * 10);

}

void Desenha::renderiza(Campo campo) {
	//Desenha a grama do campo, um grande retangulo verde
	SDL_Rect fillRect = { (int)campo.getX() / 12, (int)campo.getY() / 8, (int)campo.getX() - 2 * (int)campo.getX() / 12, (int)campo.getY() - 2 * (int)campo.getY() / 8 };
	SDL_SetRenderDrawColor(renderizador, 0x00, 0xFF, 0x00, 0xFF);
	SDL_RenderFillRect(renderizador, &fillRect);

	//Render white intlined quad
	SDL_Rect outlineRect = { (int)((campo.getX() / 12) * 1.25), (int)((campo.getY() / 8) * 1.25), (int)(campo.getX() - 2 * (int)((campo.getX() / 12) * 1.25)),(int)(campo.getY() - 2 * (int)(campo.getY() / 8) * 1.25) };
	SDL_SetRenderDrawColor(renderizador, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderDrawRect(renderizador, &outlineRect);

	//Draw white horizontal line
	SDL_SetRenderDrawColor(renderizador, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderDrawLine(renderizador, (int)(campo.getX() / 2.0), (int)((campo.getY() / 8) * 1.25), (int)(campo.getX() / 2), (int)(campo.getY() - 1.0 * (campo.getY() / 8) * 1.25));

	//Desenha o circulo central
	DrawCircle(renderizador, campo.getX() / 2, campo.getY() / 2, 50);
}

void Desenha::renderiza(Texto texto) {
	if (texto.getVisivel()) {
		// cria uma fonte com o arquivo de fonte e o tamanho detrminado no objeto texto recebido
		TTF_Font* fonte = TTF_OpenFont(texto.getFonte() , texto.getTamanho_fonte());
		if (!fonte) {
			cout << "erro na fonte" << endl;
			cout << texto.getFonte() << " " << texto.getTamanho_fonte() << endl;
			//TTF_SetError("Loading failed :( (code: %d)", 142);
			cout << "Error: " << "verifique se o arquivo l_10646.ttf esta localizado no mesmo diretório do executavel"  << endl;
			system("pause");	
			}
		else {
			// cria uma superfice com o conteudo, cor do objeto texto com a fonte recem criada
			SDL_Surface* surface = TTF_RenderText_Blended(fonte, texto.getConteudo().c_str(), texto.getCor());
			// cria uma textura coma superficie criada
			SDL_Texture* texture = SDL_CreateTextureFromSurface(renderizador, surface);
			// variaveis que abrigaram o tamanho necessario para abrigar a textura
			int tam_x;
			int tam_y;
			// determina o tamanho ideal
			SDL_QueryTexture(texture, NULL, NULL, &tam_x, &tam_y);
			// determina onde (na tela) a textura ser� renderizada e o tamanho
			SDL_Rect dstrect = { (int)texto.getPosicao().vX, (int)texto.getPosicao().vY , tam_x, tam_y };
			// renderiza a textura 
			SDL_RenderCopy(renderizador, texture, NULL, &dstrect);
			//libera espa�o
			SDL_DestroyTexture(texture);
			SDL_FreeSurface(surface);
		}
		//libera o espa�o alocado
		TTF_CloseFont(fonte);
	}
}

void Desenha::renderiza(Goleira goleira) {
	if (goleira.getLado() == 'd') {
		//trave de cima
		SDL_Rect fillRect = {(int) (goleira.getPosicao().vX), (int)(goleira.getPosicao().vY - goleira.getY()),(int)(goleira.getX()), (int)(goleira.getEspessura()) };
		SDL_SetRenderDrawColor(renderizador, 255, 255, 255, 255);
		SDL_RenderFillRect(renderizador, &fillRect);

		//trave de baixo
		SDL_Rect fillRect2 = { (int)goleira.getPosicao().vX,(int)goleira.getPosicao().vY + (int)goleira.getY(), (int)goleira.getX(), (int)goleira.getEspessura() };
		SDL_SetRenderDrawColor(renderizador, 255, 255, 255, 255);
		SDL_RenderFillRect(renderizador, &fillRect2);

		//trave de traz
		SDL_Rect fillRect3 = { (int)goleira.getPosicao().vX + (int)goleira.getX(), (int)goleira.getPosicao().vY - (int)goleira.getY(), (int)goleira.getEspessura(), 2 * (int)goleira.getY() + (int)goleira.getEspessura() };
		SDL_SetRenderDrawColor(renderizador, 255, 255, 255, 255);
		SDL_RenderFillRect(renderizador, &fillRect3);
	}
	if (goleira.getLado() == 'e') {
		//trave de cima
		SDL_Rect fillRect = { (int)goleira.getPosicao().vX, (int)goleira.getPosicao().vY - (int)goleira.getY(), (int)-goleira.getX(), (int)goleira.getEspessura() };
		SDL_SetRenderDrawColor(renderizador, 255, 255, 255, 255);
		SDL_RenderFillRect(renderizador, &fillRect);

		//trave de baixo
		SDL_Rect fillRect2 = { (int)goleira.getPosicao().vX, (int)goleira.getPosicao().vY + goleira.getY(), -goleira.getX(), goleira.getEspessura() };
		SDL_SetRenderDrawColor(renderizador, 255, 255, 255, 255);
		SDL_RenderFillRect(renderizador, &fillRect2);

		//trave de traz
		SDL_Rect fillRect3 = { (int)goleira.getPosicao().vX - (int)goleira.getX(), (int)goleira.getPosicao().vY - (int)goleira.getY(), (int)-goleira.getEspessura(), 2 * (int)goleira.getY() + (int)goleira.getEspessura() };
		SDL_SetRenderDrawColor(renderizador, 255, 255, 255, 255);
		SDL_RenderFillRect(renderizador, &fillRect3);
	}
}

void Desenha::renderiza(Placar placar, Time time_1, Time time_2) {
	//base 
	SDL_Rect fillRect = { (int)placar.getPosicao().vX - (int)placar.getPosicao().vX/4, (int)placar.getPosicao().vY, (int)placar.getPosicao().vX/2 , (int)placar.getPosicao().vX/6};
	SDL_SetRenderDrawColor(renderizador, 90, 90, 90, 40);
	SDL_RenderFillRect(renderizador, &fillRect);

	//quadrado time 1
	SDL_Rect fillRect2 = { (int)placar.getPosicao().vX - (int)placar.getPosicao().vX / 4, (int)placar.getPosicao().vY, (int)placar.getPosicao().vX / 6 , (int)placar.getPosicao().vX / 6 };
	SDL_SetRenderDrawColor(renderizador, time_1.r, time_1.g, time_1.b, 255);
	SDL_RenderFillRect(renderizador, &fillRect2);
	renderiza(placar.getG1());

	//trave de traz
	SDL_Rect fillRect3 = { (int)placar.getPosicao().vX + (int)placar.getPosicao().vX / 4, (int)placar.getPosicao().vY, (int)-placar.getPosicao().vX / 6 , (int)placar.getPosicao().vX / 6 };
	SDL_SetRenderDrawColor(renderizador, time_2.r, time_2.g, time_2.b, 255);
	SDL_RenderFillRect(renderizador, &fillRect3);
	renderiza(placar.getG2());
}

void Desenha::renderiza_forca(Texto texto, double forca) {
	
	SDL_Rect barra = { (int)texto.getPosicao().vX ,(int)texto.getPosicao().vY + 30 ,(int)forca / 5 * 15 , 20 };
	SDL_SetRenderDrawColor(renderizador, 255, 255 - (float)forca / 5 * 25, 0, 0);
	SDL_RenderFillRect(renderizador, &barra);

	SDL_Rect contorno_barra = { 9 , 39 , 151 , 21 };
	SDL_SetRenderDrawColor(renderizador, 0, 0, 0, 0);
	SDL_RenderDrawRect(renderizador, &contorno_barra);
	
}


#endif // DESENHA_H_INCLUDED

#pragma once
