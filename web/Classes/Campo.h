#ifndef CAMPO_H_INCLUDED
#define CAMPO_H_INCLUDED
#include "Inanimado.h"
using namespace std;

class Campo :public Inanimado {
private:
    int tamX;
    int tamY;

public:
    Campo(int _tamX, int _tamY);
    int getX();
    int getY();
};

Campo::Campo(int _tamX, int _tamY) {
    tamX = _tamX;
    tamY = _tamY;
    posicao = Vetor2D();
    visivel = true;
}

int Campo::getX() {
    return tamX;
}

int Campo::getY() {
    return tamY;
}

#endif 
#pragma once
