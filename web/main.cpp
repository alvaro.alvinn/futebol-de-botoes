// utilizando SDL, SDL image, E/S bésica, math, vector e SDL ttf.
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mouse.h>
#include <stdio.h>
#include <string>
#include <cmath>
#include <vector>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <fstream>

#include <emscripten.h>

// utilizando classes e funcoes criadas
#include "Classes/Vetor2D.h"
#include "Classes/Animado.h"
#include "Classes/Inanimado.h"
#include "Classes/Botao.h"
#include "Classes/Campo.h"
#include "Classes/Placar.h"
#include "my_functions.h"
#include "Classes/Desenha.h"
#include "Classes/Colisao.h"
#include "Classes/Time.h"
#include "Classes/Mouse.h"
#include "Classes/Goleira.h"

struct context {
//Cosntantes importantes
int SCREEN_WIDTH = 800;
int SCREEN_HEIGHT = 600;
int RAIO_J = 25;

//The window we'll be rendering to
SDL_Window* gWindow = NULL;

//The window renderer
SDL_Renderer* gRenderer = NULL;


	//Event handler
	SDL_Event e;
	SDL_Event ultimo_e;

	//variaveis relacionadas ao tempo
	Uint32 tempo_atual;
	Uint32 tempo_antigo;
	Uint32 tempo_em_s;
	Uint32 tempo_em_s_salvo = 0;
	string seg;
	string min;

	//variaveis de estado do jogo
	string estado_rpincipal;

	//o jogo sempre inicia com o jogador 1 
	//Opções: vez_de_1, vez_de_2, pronto_para_jogar, movimento;
	string sub_estado;
	string proximo_sub_estado;

	//Instancia Mouse
	Mouse* mouse;

	// Instancia objeto desenha
	Desenha* renderizador;

	// Inicia Colisao
	Colisao* colisao;

	//Instancia times
	Time* time_1;
	Time* time_2;
	Time* time_bola;

	//Instancia placar
	Placar* placar;

	// Instancia objetos botÃO
	Botao* jogador_1;
	Botao* jogador_2;
	Botao* jogador_3;
	Botao* jogador_4;

	Botao* jogador_5;
	Botao* jogador_6;
	Botao* jogador_7;
	Botao* jogador_8;
	
	//Instancia bola
	Botao* bola;

	//Vector com todos os ativos (botoese bola)
	vector<Botao> ativos;

	size_t total;
	int a;

	// Instancia objeto campo
	Campo* campo;
	// Instancia goleiras
	Goleira* goleira_D;
	Goleira* goleira_E;
	Goleira* goleira_proxima;

	// Instancia Textos
	Texto* texto_superior_esquerda;
	Texto* relogio;

	Texto* info;
	Texto* jogue;
	Texto* continuar;

	// inicia variaveis importantes
	bool pronto_para_jogar;
	int j_da_vez;
	int jogador_atual;
	bool fim_de_jogada;

	//arquvo de save
	fstream savefile;

	// foréa: de 1 até 50 !!
	bool mais_forca;
	double forca;

};

//Inicia o SDL e cira uma janela
bool init();

//Loads media
bool loadMedia();

//Frees media and shuts down SDL
void close();

//Loads individual image as texture
SDL_Texture* loadTexture( std::string path );

int main(int argc, char* args[]);

void mainloop(void *arg);

void loop (context* ctx);



void init(context *ctx)
{
	//Initialization flag
	bool success = true;
	if (TTF_Init() < 0) {
		printf("NÃO foi possivel inicializar SDL_TTF!, algumas funéées podem não funcionar");
	}

	ctx->tempo_antigo = SDL_GetTicks(), ctx->tempo_atual;
	ctx->tempo_em_s_salvo = 0;

	ctx->sub_estado = "inicio";
	ctx->proximo_sub_estado = "inicio";

	//Instancia Mouse
	ctx->mouse = new Mouse();

	// Instancia objeto desenha
	ctx->renderizador = new Desenha(ctx->gRenderer);

	// Inicia Colisao
	ctx->colisao = new Colisao();

	//Instancia times
	ctx->time_1 = new Time("Time 1", 40, 180, 200);
	ctx->time_2 = new Time("Time 2", 199, 72, 50);
	ctx->time_bola = new Time("", 89, 89, 83);

	//Instancia placar
	ctx->placar = new Placar(ctx->SCREEN_WIDTH/2);

	// Instancia objetos botÃO
	ctx->jogador_1 = new Botao((int)(ctx->SCREEN_WIDTH / 12 + ctx->SCREEN_WIDTH / 12), ctx->SCREEN_HEIGHT / 2, ctx->RAIO_J, ctx->time_1, 50, true);
	ctx->jogador_2 = new Botao((int)(ctx->SCREEN_WIDTH / 12 + ctx->SCREEN_WIDTH / 5) , 2* ctx->SCREEN_HEIGHT / 8 , ctx->RAIO_J, ctx->time_1, 50, true);
	ctx->jogador_3 = new Botao((int)(ctx->SCREEN_WIDTH / 12 + ctx->SCREEN_WIDTH / 5), (int)(ctx->SCREEN_HEIGHT - 2 * ctx->SCREEN_HEIGHT / 8 ) , ctx->RAIO_J, ctx->time_1, 50, true);
	ctx->jogador_4 = new Botao((int)(ctx->SCREEN_WIDTH / 12 + ctx->SCREEN_WIDTH / 3.5) , ctx->SCREEN_HEIGHT / 2, ctx->RAIO_J, ctx->time_1, 50, true);

	ctx->jogador_5 = new Botao((int)(ctx->SCREEN_WIDTH - (ctx->SCREEN_WIDTH/12 + ctx->SCREEN_WIDTH / 12)), ctx->SCREEN_HEIGHT /2, ctx->RAIO_J, ctx->time_2, 50, true);
	ctx->jogador_6 = new Botao((int)(ctx->SCREEN_WIDTH - (ctx->SCREEN_WIDTH / 12 + ctx->SCREEN_WIDTH / 5 )), 2* ctx->SCREEN_HEIGHT / 8 , ctx->RAIO_J, ctx->time_2, 50, true);
	ctx->jogador_7 = new Botao((int)(ctx->SCREEN_WIDTH - (ctx->SCREEN_WIDTH / 12 + ctx->SCREEN_WIDTH / 5)), (int)(ctx->SCREEN_HEIGHT - 2 * ctx->SCREEN_HEIGHT / 8  ), ctx->RAIO_J, ctx->time_2, 50, true);
	ctx->jogador_8 = new Botao((int)(ctx->SCREEN_WIDTH - (ctx->SCREEN_WIDTH / 12 + ctx->SCREEN_WIDTH / 3.5)) , ctx->SCREEN_HEIGHT / 2, ctx->RAIO_J, ctx->time_2, 50, true);
	
	//Instancia bola
	ctx->bola = new Botao(ctx->SCREEN_WIDTH/2, ctx->SCREEN_HEIGHT/2, 10, ctx->time_bola, 20, true);

	//Vector com todos os ativos (botoese bola)
	ctx->ativos.push_back(*ctx->jogador_1);
	ctx->ativos.push_back(*ctx->jogador_2);
	ctx->ativos.push_back(*ctx->jogador_3);
	ctx->ativos.push_back(*ctx->jogador_4);
	ctx->ativos.push_back(*ctx->jogador_5);
	ctx->ativos.push_back(*ctx->jogador_6);
	ctx->ativos.push_back(*ctx->jogador_7);
	ctx->ativos.push_back(*ctx->jogador_8);

	ctx->ativos.push_back(*ctx->bola);

	ctx->total = ctx->ativos.size();
	ctx->a = 0; //id do jogador ativo IMPORTANTE

	// Instancia objeto campo
	ctx->campo = new Campo(ctx->SCREEN_WIDTH, ctx->SCREEN_HEIGHT);
	// Instancia goleiras
	ctx->goleira_D = new Goleira('d', 35, 65, 7, Vetor2D((int)(ctx->SCREEN_WIDTH - (ctx->SCREEN_WIDTH / 12)), ctx->SCREEN_HEIGHT / 2));
	ctx->goleira_E = new Goleira('e', 35, 65, 7, Vetor2D(ctx->SCREEN_WIDTH / 12, ctx->SCREEN_HEIGHT / 2));

	// Instancia Textos
	ctx->texto_superior_esquerda = new Texto("FORÇA:", 10, 10, true);
	ctx->relogio = new Texto("00:00", ctx->SCREEN_WIDTH / 2 - 28, 40, true);

	ctx->info = new Texto("", ctx->SCREEN_WIDTH / 2 - 220, ctx->SCREEN_HEIGHT - 50, false);
	ctx->jogue = new Texto("JOGUE!", ctx->SCREEN_WIDTH / 2 - 30, ctx->SCREEN_HEIGHT - 50, false);
	ctx->continuar = new Texto("GOSTARIA DE CONTINUAR PARTIDA ANTERIOR? S/N", ctx->SCREEN_WIDTH / 5.5 , ctx->SCREEN_HEIGHT/1.7, true);

	// inicia variaveis importantes
	ctx->pronto_para_jogar = true;
	ctx->j_da_vez = 1;
	ctx->jogador_atual = 0;
	ctx->fim_de_jogada = false;

	// foréa: de 1 até 50 !!
	ctx->mais_forca = true;
	ctx->forca = 0;
}

bool loadMedia()
{
	//Loading success flag
	bool success = true;

	//Nothing to load
	return success;
}

void close(context *ctx)
{
 	SDL_DestroyRenderer(ctx->gRenderer);
	SDL_Quit();
}

SDL_Texture* loadTexture( std::string path, context* ctx)
{
	//The final texture
	SDL_Texture* newTexture = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load( path.c_str() );
	if( loadedSurface == NULL )
	{
		printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
	}
	else
	{
		//Create texture from surface pixels
        newTexture = SDL_CreateTextureFromSurface( ctx->gRenderer, loadedSurface );
		if( newTexture == NULL )
		{
			printf( "Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
		}

		//Get rid of old loaded surface
		SDL_FreeSurface( loadedSurface );
	}

	return newTexture;
}


int main( int argc, char* args[] )
{	
	context ctx;
	SDL_Init(SDL_INIT_VIDEO);
	ctx.SCREEN_WIDTH = 800;
	ctx.SCREEN_HEIGHT = 600;
	ctx.RAIO_J = 25;
	
	SDL_Window *window;
	SDL_SetWindowTitle(window, "Futebol de Botoes");
	SDL_Renderer * renderer;
	SDL_CreateWindowAndRenderer(ctx.SCREEN_WIDTH, ctx.SCREEN_HEIGHT, 0, &window, &renderer);
	ctx.gRenderer = renderer;
	ctx.gWindow = window;

	init(&ctx);

	emscripten_set_main_loop_arg(mainloop, &ctx, -1, 1);

	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(renderer);
	close(&ctx);
}

void mainloop(void *arg) {
 context *ctx = static_cast<context*>(arg);
 loop(ctx);
}

void loop (context* ctx)
{
	bool clicou = false;
	//variaveis auxiliares
	int i = 0;
	int j = 0;
	int k = 0;
	// Gerencia tempo
	if (ctx->sub_estado != "inicio") {
		ctx->tempo_atual = SDL_GetTicks();
		ctx->tempo_em_s = ((ctx->tempo_atual - ctx->tempo_antigo) / 1000) + ctx->tempo_em_s_salvo;
		i = (int)(ctx->tempo_em_s / 60);
		j = (int)((int)ctx->tempo_em_s % 60);
		if (i > 9) {
			ctx->min = to_string(i);
		}
		else {
			ctx->min = "0" + to_string(i);
		}
		if (j > 9) {
			ctx->seg = to_string(j);
		}
		else {
			ctx->seg = "0" + to_string(j);
		}
		ctx->relogio->setConteudo(ctx->min + ":" + ctx->seg);
	}

	//gerenciador de eventos
	while (SDL_PollEvent(&ctx->e) != 0)
	{

		if(ctx->e.type == SDL_MOUSEBUTTONUP){
			clicou = true;
			std::cout << "CLICLOU" << std::endl;
		}

		if (ctx->sub_estado == "inicio") {
			if (ctx->e.type == SDL_KEYDOWN) {
				if (ctx->e.key.keysym.sym == SDLK_s) {
					string x;
					string y;
					string aux;

					ctx->savefile.open("ultimo_jogo.txt", ios::in);
					if (ctx->savefile.is_open()) {
						cout << "ARQUIVO ABERTO COM SUCESSO!\n";
						//recupera posicao dos jogadores
						for (i = 0; i < ctx->total; i++) {
							for (k = 0; k < 2; k++) {
								if (k == 0) {
									getline(ctx->savefile, x);
								}
								else {
									getline(ctx->savefile, y);
								}
							}
							ctx->ativos[i].setPosicao(stod(x), stod(y));
						}
						//recupera pontuacao do time 1
						getline(ctx->savefile, aux);
						ctx->placar->setG1(stoi(aux));
						//recupera pontuacao do time 2
						getline(ctx->savefile, aux);
						ctx->placar->setG2(stoi(aux));
						//recupera tempo de jogo
						getline(ctx->savefile, aux);
						ctx->tempo_em_s_salvo = (Uint32)stoi(aux);
						//recupera de quem e a vez de jogar
						getline(ctx->savefile, aux);
						ctx->sub_estado = aux;

						ctx->continuar->setVisivel(false);
						ctx->savefile.close();
					}
					else {
						cout << "Erro ao abrir o arquivo ou nao ha registros de jogo anterior";
						ctx->sub_estado = "vez_de_1";
						ctx->continuar->setVisivel(false);
					}
				}
				else if(ctx->e.key.keysym.sym == SDLK_n) {
					ctx->sub_estado = "vez_de_1";
					ctx->continuar->setVisivel(false);
				}
			}
		}
		//savla o ultimo evento
		ctx->ultimo_e = ctx->e;
	}

	if (ctx->sub_estado == "vez_de_1") {
		ctx->proximo_sub_estado = "vez_de_1";
		ctx->forca = 0;
		ctx->info->setConteudo("VEZ DO JOGADOR 1 SELECIONAR O BOTAO!");
		ctx->info->setVisivel(true);
		ctx->jogador_atual = 1;
		// o mais proximo do mouse é o ativo
		double menor = 100000;
		//backspace para voltar uma jogada
		for (i = 0; i < (ctx->total - 1); i++) {
			if (ctx->ativos[i].getTime().nome == "Time 1") {
				if (ctx->mouse->distancia(ctx->ativos[i]) < menor) {
					menor = ctx->mouse->distancia(ctx->ativos[i]);
				
					ctx->ativos[ctx->a].setAtivo(false);
					
					ctx->a = i;
				}
			}
		}
		ctx->ativos[ctx->a].setAtivo(true);
		//se o clique ocorre o jogador mais proximo é selecionado 
		if (clicou) {
			ctx->proximo_sub_estado = "pronto_para_jogar";
			ctx->pronto_para_jogar = false;
		}
	}
	if (ctx->sub_estado == "vez_de_2") {
		ctx->proximo_sub_estado = "vez_de_2";
		ctx->forca = 0;					
		ctx->info->setConteudo("VEZ DO JOGADOR 2 SELECIONAR O BOTAO!");
		ctx->info->setVisivel(true);
		ctx->jogador_atual = 2;
		// o mais proximo do mouse é o ativo
		double menor = 100000;
		for (i = 0; i < (ctx->total - 1); i++) {
			if (ctx->ativos[i].getTime().nome == "Time 2") {
				if (ctx->mouse->distancia(ctx->ativos[i]) < menor) {
					menor = ctx->mouse->distancia(ctx->ativos[i]);
					ctx->ativos[ctx->a].setAtivo(false);
					ctx->a = i;
				}
			}
		}
		ctx->ativos[ctx->a].setAtivo(true);
		//se o clique ocorre o jogador mais proximo é selecionado 
		if (clicou) {
			ctx->proximo_sub_estado = "pronto_para_jogar";
			ctx->pronto_para_jogar = false;
		}
	}
	if (ctx->sub_estado == "pronto_para_jogar") {
		ctx->proximo_sub_estado = "pronto_para_jogar";
		ctx->info->setVisivel(false);
		ctx->jogue->setVisivel(true);
		if (ctx->e.type == SDL_MOUSEBUTTONDOWN) {
			ctx->pronto_para_jogar = true;
		}
		// MOVIMENTO POR CLICK DO MOUSE					
		for (i = 0; i < ctx->total; i++) {
			// iteração somente com o botão selecionado como ativo pelo usuario
			if (ctx->ativos[i].getAtivo()) {
				if ((clicou)) {
					ctx->ativos[i].setVelocidade(ctx->mouse->Impulso(ctx->ativos[i], ctx->forca));
					ctx->pronto_para_jogar = false;
					ctx->ativos[i].setAtivo(false);
					ctx->proximo_sub_estado = "movimento";
					ctx->jogue->setVisivel(false);					
					cout << ctx->ativos[0].getMovimento() << endl;
				}
			}
		}
		//TRATAMENTO DA FORÇA
		if (!ctx->jogador_1->getMovimento()) {
			if (ctx->mais_forca) {
				if (ctx->forca >= 49) {
					ctx->forca -= 1;
					ctx->mais_forca = false;
				}
				ctx->forca += 1;
			}
			else {
				if (ctx->forca <= 1) {
					ctx->forca += 1;
					ctx->mais_forca = true;
				}
				ctx->forca -= 1;
			}
		}
	}
	// ESTADO DE MOVIMENTO
	if (ctx->sub_estado == "movimento") {
		ctx->proximo_sub_estado = "movimento";
		//FÍSICA ENTRE OBJETOS
		ctx->fim_de_jogada = false;
		bool todos_parados = true;
		for (i = 0; i < ctx->total; i++) {
			
			//define a goleira mais proxima
			if (ctx->ativos[i].getPosicao().vX > ctx->SCREEN_WIDTH / 2) {
				ctx->goleira_proxima = ctx->goleira_D;
			}
			else {
				ctx->goleira_proxima = ctx->goleira_E;
			}
			
			//teste de colisÃO com a goleira
			if (ctx->colisao->esta_dentro(ctx->ativos[i], *ctx->goleira_proxima)) {
				ctx->colisao->colide(ctx->ativos[i], *ctx->goleira_proxima);
			}
			//caso não colida com a goleira teste de colisao com o campo
			else {
				ctx->colisao->colide(ctx->ativos[i], *ctx->campo);
			}
			//detecção de colisao entre jogadores 
			for (j = i + 1; j < ctx->total; j++) {
				//printf("b\n");
				if (ctx->colisao->colide(ctx->ativos[i], ctx->ativos[j])) {
					cout << endl;
					ctx->colisao->Impulso(ctx->ativos[i], ctx->ativos[j]);
				}
			}

			// movimenta 
			if (ctx->ativos[i].getMovimento()) {
				//printf("c\n");
				ctx->ativos[i].movimenta();
			}

			//determina se não ha mais moviemnto
			if (ctx->ativos[i].getMovimento() && todos_parados) {
				todos_parados = false;
			}
			
			//ultimo movimento do frame
			if (i == (ctx->total - 1)) {
				//detecção de gol
				if (ctx->colisao->esta_inteiro_dentro(ctx->ativos[i], *ctx->goleira_proxima)) {
					cout << ctx->goleira_proxima->getLado() << endl;
					if (ctx->goleira_proxima->getLado() == 'd') {
						ctx->placar->aumentaG1();
						ctx->proximo_sub_estado = "vez_de_2";
						//para não entrar no próximo if
						todos_parados = false;
					}
					else if (ctx->goleira_proxima->getLado() == 'e') {
						ctx->placar->aumentaG2();
						ctx->proximo_sub_estado = "vez_de_1";
						//para não entrar no próxmo if
						todos_parados = false;

					}
					for (k = 0; k < ctx->total; k++) {
						ctx->ativos[k].setPosPadrao();
					}
					ctx->fim_de_jogada = true;
				}
				//se estão todos paradao sai do estado de moviemnto
				if (todos_parados) {
					if (ctx->jogador_atual == 1) {
						ctx->proximo_sub_estado = "vez_de_2";
					}
					else {
						ctx->proximo_sub_estado = "vez_de_1";
					}
					ctx->fim_de_jogada = true;
				}
			}
		}
		//salva 
		if (ctx->fim_de_jogada) {
			ctx->savefile.open("ultimo_jogo.txt", ios::out);
			//grava posicoes dos bototes
			for (k = 0; k < ctx->total; k++) {
				ctx->savefile << ctx->ativos[k].para_string();
			}
			//grava gols
			ctx->savefile << ctx->placar->getG1().getConteudo() << "\n" << ctx->placar->getG2().getConteudo() << endl;
			//grava tempo
			ctx->savefile << ctx->tempo_em_s << endl;
			//grava sub estado (de  quem é a vez de jogar)
			ctx->savefile << ctx->sub_estado;
			ctx->savefile.close();
		}
		//savla o ultimo evento
		ctx->ultimo_e = ctx->e;
	}				
	//converte o valor da força para seu objeto de texto
	ctx->texto_superior_esquerda->setConteudo(string("FORCA: ") + to_string((int)ctx->forca) + "." + to_string((int)((ctx->forca - (int)ctx->forca) * 10)));

	ctx->sub_estado = ctx->proximo_sub_estado;

	//limpa a tela para desenhar o próximo frame, com o fundo cinza
	SDL_SetRenderDrawColor(ctx->gRenderer, 0x80, 0x80, 0x80, 0x80);
	SDL_RenderClear(ctx->gRenderer);

	//desenha o campo
	ctx->renderizador->renderiza(*ctx->campo);

	//desenha o placar
	ctx->renderizador->renderiza(*ctx->placar, *ctx->time_1, *ctx->time_2);

	//desenha goleiras
	ctx->renderizador->renderiza(*ctx->goleira_D);
	ctx->renderizador->renderiza(*ctx->goleira_E);

	//desenha o jogadores
	for (i = 0; i < ctx->total; i++) {
		if (ctx->ativos[i].getVisivel()) {
			ctx->renderizador->renderiza(ctx->ativos[i]);
		}
	}

	//desenha textos
	ctx->renderizador->renderiza(*ctx->texto_superior_esquerda);
	ctx->renderizador->renderiza(*ctx->relogio);

	//desenha barra de força
	ctx->renderizador->renderiza_forca(*ctx->texto_superior_esquerda, ctx->forca);

	ctx->renderizador->renderiza(*ctx->info);
	ctx->renderizador->renderiza(*ctx->jogue);
	ctx->renderizador->renderiza(*ctx->continuar);

	//Atualiza a tela
	SDL_RenderPresent(ctx->gRenderer);
	
}