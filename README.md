# Futebol de botões

Um jogo simples de futebol de botões desenvolvido em C++ com a biblioteca SDL2.

## Em sua versão final o jogo deve cumprir os seguintes requisitos:


- [x] Interface gráfica em 2D que abranja todos os elementos do jogo;

- [x] Simulação de colisão entre botões, objeto goleiro, bola, goleira e limites do campo (colisão e movimento);

- [x] Possibilidade de continuar a partida anterior.
